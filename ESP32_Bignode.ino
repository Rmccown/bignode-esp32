
/**
  This is the ESP32 version of my CMRI-enabled local node processor.

  Combined with the ESP32_RemoteNode sketch, it provides I/O pins without running
  large amount of wiring all over your layout.  You can have nodes at each
  interlocking transmitting back switch position and occupancy information, 
  and setting signal aspects, all powered either from a 12v bus, or even
  rechargable batteries.

  This should run on any ESP32 module with minor modifications.  It has been tested on an EZSBC development board.

 */

#include "namedMesh.h"
#include <CMRI.h>
#include <LiquidCrystal_I2C.h>
#include <Pushbutton.h>

#define   LED             2

#define   MESH_SSID       "whateverYouLike"
#define   MESH_PASSWORD   "somethingSneaky"
#define   MESH_PORT       5555

//  Remove this after development
#define DEBUG

#define VERSION "0.92"

char VersionNum[] = "0.92 ";
char VersionDate[] = "05-31-2021";

#ifndef JMRI_BAUDRATE  // make sure this matches your speed set in JMRI
#define JMRI_BAUDRATE 9600
#endif

// Prototypes
//void pingRemoteNodes(); 
//void receivedCallback(uint32_t from, String & msg);
//void newConnectionCallback(uint32_t nodeId);
//void changedConnectionCallback(); 
//void nodeTimeAdjustedCallback(int32_t offset); 
//void delayReceivedCallback(uint32_t from, int32_t delay);

Scheduler userScheduler;
namedMesh mesh;

//  From the SMINI docs:  a single SMINI can drive 48 signal LEDs and read 24 detector 

//  Code supports max 254 nodes 
//  Each remote node has 12 outputs and 6 inputs.

#define NUMBER_REMOTE_NODES 10

//  One giant CMRI node of up to 2048 pins
CMRI cmri(0, NUMBER_REMOTE_NODES * 6, NUMBER_REMOTE_NODES * 12);

//  Input and Output bit storage
//  These should probably be in EEPROM
bool localInputBits[NUMBER_REMOTE_NODES * 6];
bool localOutputBits[NUMBER_REMOTE_NODES * 12];

//  display setup
LiquidCrystal_I2C lcd(0x27, 20, 4);

//  buttons
Pushbutton buttonUp(13);
Pushbutton buttonDown(12);
int displayIndex = 0;

bool calc_delay = false;
SimpleList<uint32_t> nodes;

void pingRemoteNodes() ; // Prototype
Task taskPingRemoteNodes( 500, TASK_FOREVER, &pingRemoteNodes ); // half second interval - shorten until explosion

bool onFlag = false;

String nodeName = "RootNode";

void setup() {
  Serial.begin(JMRI_BAUDRATE, SERIAL_8N2);

  pinMode(LED, OUTPUT);

#ifdef DEBUG
  //  Pick one of these
  //mesh.setDebugMsgTypes( ERROR | MESH_STATUS | CONNECTION | SYNC | COMMUNICATION | GENERAL | MSG_TYPES | REMOTE ); // all types on
  //mesh.setDebugMsgTypes(ERROR | DEBUG | CONNECTION | COMMUNICATION);  // set before init() so that you can see startup messages
  //mesh.setDebugMsgTypes(ERROR | DEBUG | CONNECTION);  // set before init() so that you can see startup messages
  //mesh.setDebugMsgTypes( CONNECTION | SYNC );
#endif

  mesh.init(MESH_SSID, MESH_PASSWORD, &userScheduler, MESH_PORT);
  mesh.onReceive(&receivedCallback);
  mesh.onNewConnection(&newConnectionCallback);
  mesh.onChangedConnections(&changedConnectionCallback);
  mesh.onNodeTimeAdjusted(&nodeTimeAdjustedCallback);
  mesh.onNodeDelayReceived(&delayReceivedCallback);

  //  Set root Node Name
  mesh.setName(nodeName);

  userScheduler.addTask( taskPingRemoteNodes );
  taskPingRemoteNodes.enable();

  randomSeed(analogRead(A0));

  //  Initialize display
  lcd.init();
  lcd.on();
  lcd.backlight();
  lcd.setCursor(0,0);
  lcd.print("ESP32-C/MRI BigNode");
  lcd.setCursor(0,2);
  lcd.print("Version");
  lcd.setCursor(8,2);
  lcd.print(VersionNum);
  lcd.setCursor(0,3);
  lcd.print(VersionDate);
  lcd.print(" build");
  delay(3600);
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Expected Nodes:  ");
  lcd.print(NUMBER_REMOTE_NODES);  
  lcd.setCursor(0,1);
  lcd.print("Connected Nodes: "); 
  lcd.print(nodes.size());    
}

//  Main loop

void loop() {
  userScheduler.execute(); // it will run mesh scheduler as well
  mesh.update();
  //  Process all CMRI nodes
  cmri.process();
  //  Set output data
  for(int j=0;j<48;j++) {
    localOutputBits[j] = cmri.get_bit(j);
  }
  //  Push input data
  for(int j=0;j<24;j++) {
    cmri.set_bit(j,localInputBits[j]);
  }    
  //  Toggle LED
  digitalWrite(LED, !onFlag);
  //  Button housekeeping
  if( buttonDown.getSingleDebouncedPress() ) {
  }
  if (buttonUp.getSingleDebouncedPress() ) {
  }
}

String readBits() {
  String ptr = "";
  char szCMRI[10];
  char szBits[100];
  memset(szBits,0,sizeof(szBits));
  sprintf(szCMRI,"CMRI: 0");
  ptr += szCMRI;
  for(int j=0;j<48;j++) {
    szBits[j] = localOutputBits[j] ? 'T' : 'F';
  }
  szBits[48] = ' ';
  szBits[49] = ' ';
  for(int j=0;j<24;j++) {
    szBits[j+50] = localInputBits[j] ? 'T' : 'F';
  }
  ptr += szBits;
  ptr += "<br>";
  return ptr;
}

void pingRemoteNodes() {
  char nodeId[10];
  //  We need to assemble a packet for each node we think is in the system
  for( int n1 = 0; n1 < NUMBER_REMOTE_NODES; n1++ ) {
    int cmriNodeNumber = n1 + (NUMBER_REMOTE_NODES);
    sprintf(nodeId,"Node%d",cmriNodeNumber);
    String to = nodeId;
    String pins = "";
    String msg = "";
    for(int j=0;j<12;j++) {
      pins += localOutputBits[(n1)*12+j] ? '1' : '0';
    }
    StaticJsonDocument<200> doc;
    doc["node"] = cmriNodeNumber;
    doc["pins"] = pins;
    serializeJson(doc, msg);

    mesh.sendSingle(to, msg);
#ifdef DEBUG
    //Serial.printf("Sending message to %s: %s\n", to.c_str(), msg.c_str());
#endif
  }

  if (calc_delay) {
    SimpleList<uint32_t>::iterator node = nodes.begin();
    while (node != nodes.end()) {
      mesh.startDelayMeas(*node);
      node++;
    }
    calc_delay = false;
  }
}

void receivedCallback(String &from, String &msg) {
  //  format: {node: "Node0", pins: '100101'}
  StaticJsonDocument<200> doc;
  DeserializationError error = deserializeJson(doc, msg);
  if (error) {
#ifdef DEBUG
    Serial.print(F("Message decoding failure: "));
    Serial.println(error.c_str());
#endif
    return;
  }
  //  TODO: Make sure from param and node ID in the packet are the same
  String node = doc["node"];
  String pins = doc["pins"];
  node.remove(0,4);
  if( node.toInt() > NUMBER_REMOTE_NODES ) {
#ifdef DEBUG
    Serial.printf("Node identifying as %s, rejected\n",node);
#endif
    return;  
  }
#ifdef DEBUG
  //Serial.printf("Decoded from %s/(Node: %s): %s - Sending to cmri\n", from, node, pins);
#endif
  //  localInputBits needs to offset by the node number
  //  (first six pins are node 0, second six are node 1, etc)
  int offset = node.toInt() * 6;
#ifdef DEBUG
  //Serial.printf("Offsetting for node %s, %d bits\n", node, offset);
#endif
  localInputBits[offset + 0] = pins[0] == '1' ? true : false;
  localInputBits[offset + 1] = pins[1] == '1' ? true : false;
  localInputBits[offset + 2] = pins[2] == '1' ? true : false;
  localInputBits[offset + 3] = pins[3] == '1' ? true : false;
  localInputBits[offset + 4] = pins[4] == '1' ? true : false;
  localInputBits[offset + 5] = pins[5] == '1' ? true : false;
}

void newConnectionCallback(uint32_t nodeId) {
  onFlag = false;
 
#ifdef DEBUG
  Serial.printf("New Connection, nodeId = %u\n", nodeId);
#endif

  nodes = mesh.getNodeList();

  lcd.setCursor(0,1);
  lcd.print("Connected Nodes: ");
  lcd.print(nodes.size());

#ifdef DEBUG
  Serial.printf("Num nodes: %d\n", nodes.size());
  Serial.printf("Connection list:");
  SimpleList<uint32_t>::iterator node = nodes.begin();
  while (node != nodes.end()) {
    Serial.printf(" %u", *node);
    node++;
  }
  Serial.println();
#endif
}

void changedConnectionCallback() {
#ifdef DEBUG
  Serial.printf("Changed connections %s\n", mesh.subConnectionJson().c_str());
#endif
  onFlag = false;
 
  nodes = mesh.getNodeList();

  lcd.setCursor(0,1);
  lcd.print("Connected Nodes: ");
  lcd.print(nodes.size());

#ifdef DEBUG
  Serial.printf("Num nodes: %d\n", nodes.size());
  Serial.printf("Connection list:");
  SimpleList<uint32_t>::iterator node = nodes.begin();
  while (node != nodes.end()) {
    Serial.printf(" %u", *node);
    node++;
  }
  Serial.println();
#endif
  calc_delay = true;
}

void nodeTimeAdjustedCallback(int32_t offset) {
  //Serial.printf("Adjusted time %u. Offset = %d\n", mesh.getNodeTime(), offset);
}

void delayReceivedCallback(uint32_t from, int32_t delay) {
  //Serial.printf("Delay to node %u is %d us\n", from, delay);
}
