# BigNode for ESP32

A CMRI node with WiFi input/output nodes.  This allows you to save on wiring, or in environments such as a garden railroad where wiring would be easily damaged.

## Table of Contents
1. [About the Project](#about-the-project)
1. [Project Status](#project-status)
1. [Getting Started](#getting-started)
1. [Roadmap](#roadmap)
1. [Caveats](#caveats)

# About the Project

This project came about from my desire to have signals on my garden railroad, and my interest in both OpenLCB, CMRI, and the JMRI software package.

It uses the ArduinoCMRI library to simulate a node for Bruce Chubb's CMRI signaling system.  But, instead of having wires run directly to the node, it uses WiFi networking, via the painlessMesh library, to control one or more remote nodes.  This will cut down on wiring, or, in my case, not have a lot of wiring exposed to the weather, frost heaves, etc, that affect my garden railroad.

I originally implemented this using an Arduino and HC12 modules, but I could not get the range or networking that I wanted.  Switching to ESP32 modules and using the painlessMesh library allowed me to concentrate on building out features using a self-assembling mesh network, instead of fighting with protocols and network issues.

I develop using the Arduino IDE (v1.8.13 at this time).  There are probably better packages out there, but I have been using it for years for various projects, and it is a comfortable friend.  If you have a package you like better, give it a whirl and let me know if it works.  

# Project Status

This project is currently under development, so we can consider it an alpha release.  It currently is not a complete system.  See the [Roadmap](#roadmap) for the path to the v1.0 release.  Use at your own risk, etc etc...

As of 4/23/2021, BigNode and RemoteNodes have been tested simulating a single SMINI with 4 remote nodes.

Until such a time as I stamp an official v1.0 release, I reserve the right to change the packets that I send throughout the network.  Currently they are JSON formatted for human readability, but I may change that.  After the v1.0 release, I will keep the packet format the same until such a time as additional features (or bugs) require it.  I will make all attempts to make any packet changes backwards-compatable.

If someone wants to take a stab at a code changes and a PR that would support this on the various Nucleo STM boards and the X-NUCLEO-IDW01M1 WiFi expansion board, that would be a fantasic win in adding a giant amount of horsepower to this project.

# Getting Started

## What to buy

I developed this on the ESP32-01 modules from [EZSBC](https://www.ezsbc.com/).  I assume this will work on any ESP32 board.  You will need at least two of these to start with.  I recommend battery powered modules [like this one](https://www.ezsbc.com/product/wifi01-cell4/) for repeater nodes.  Makes moving then around handy.
One for the BigNode (this package) and the rest for remote nodes or repeater nodes.
You will also need a 4x20 LCD display that uses I2C for connectivity.  You can use a different display, or none at all.  You will have to modify code accordingly. If you wish to supply me with the peripherals, I will attempt to make it work.  Same goes for alternate ESP32 modules with different pinouts, for use on the RemoteNode.

## Prerequisites

* Arduino IDE 1.8.13
* [painlessMesh](https://gitlab.com/painlessMesh/painlessMesh), and the [namedMesh.h](https://platformio.org/lib/show/1269/painlessMesh) file from the examples.
* [ArduinoCMRI](https://github.com/madleech/ArduinoCMRI) from Madleech.  This appears to be abandonware, and hasn't been updated in some time.  I should make my own fork for stability reasons.
* One or more [Remote Nodes](https://gitlab.com/Rmccown/remotenode-esp32)
* One or more [Repeater Nodes](https://gitlab.com/Rmccown/esp32_repeater)

## How it works

The painlessMesh library builds a mesh network of modules.  These modules learn about all the other modules in the network, and construct (and modify) a network of nodes to forward data packets to.  The mesh network is fluid, as in when a node joins or leaves, the network reconfigures itself for optimization.  There is never more than one route between two nodes on the network.

The ArduinoCMRI library simulates a CMRI node, with a default of 24 inputs and 48 outputs (SMINI).  These 'bits' are packaged and sent out to whichever node is assigned to those pins.

If you find your network is fragile, or all nodes cannot connect, you should use the extender node (to be developed)

There is a mesh visualizer for PainlessMesh available [Here](https://yoppychia.wordpress.com/2018/12/28/esp8266-painlessmesh-topology-visualizer-in-python/).  It works well, but requires a modification to the networkx/drawing/nx_pylab.py python file.  See the article for further details.

## Setup

Edit the script.  Change the following settings to reflect your reality:

| Variable | Comment |
| ------ | ------ |
| MESH_SSID | SSID for Bignode network |
| MESH_PASSWORD | Password for Bignode network | 
| MESH_PORT | Random number here |
| JMRI_BAUDRATE | Baud rate for CMRI network in JMRI |
| NUMBER_REMOTE_NODES | How many remote nodes are we going to talk to? |
| DEBUG | Setting this to `1` will cause a lot of output in the serial monitor |

Note that MESH_SSID, MESH_PASSWORD, and MESH_PORT need to be the same for all nodes (BigNode and Remotes) in a network.

Compile and load this into your ESP32 module.  Once loaded, open a serial monitor, and reset the ESP32 module.

I have a case designed for the BigNode, for 3d printing.  It is available on Thingiverse at https://www.thingiverse.com/thing:4815118

# Roadmap

Things left to do on the journey to v1.0

* Testing on 2+ remote nodes
* DRY up code, add more DEBUG serial output, etc

# Caveats

This node does not implement RS485, so it cannot comminucate on a standard CMRI bus.
